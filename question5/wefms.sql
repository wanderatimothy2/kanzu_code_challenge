-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2022 at 02:45 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wefms`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(10) NOT NULL,
  `activity` varchar(100) DEFAULT NULL COMMENT 'The task being carried out e.g creating form',
  `description` varchar(255) DEFAULT NULL COMMENT 'description of what has to happen for the action to occur',
  `status` varchar(20) DEFAULT NULL COMMENT 'status of the activity',
  `staff_id` int(10) NOT NULL COMMENT 'staff invoived in said activity',
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This saves the activity logs of every staff member as they interact with the system';

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` int(10) NOT NULL,
  `field_id` int(10) NOT NULL COMMENT 'identifies the specific form field',
  `entry_value` varchar(255) DEFAULT NULL COMMENT 'the value of the form field',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `record_id` int(10) NOT NULL COMMENT 'identifies the record to which the specific entry belongs.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is a model representing form entries to the corresponding record';

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(10) NOT NULL,
  `field_name` varchar(150) DEFAULT NULL COMMENT 'name to identify the field',
  `data_type` varchar(20) DEFAULT NULL COMMENT 'the data type of the field',
  `form_id` int(10) NOT NULL COMMENT 'form identifier',
  `is_deleted` int(1) DEFAULT 0 COMMENT 'deletion flag',
  `created_by` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='this model represents the form fields';

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `form_name` varchar(100) DEFAULT NULL COMMENT 'name of the form, must be unique to prevent duplication.',
  `description` varchar(255) DEFAULT NULL COMMENT 'description of the form (optional)',
  `created_by` int(10) NOT NULL COMMENT 'the staff member that created the form',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `form_type` varchar(60) DEFAULT NULL COMMENT 'the type of form (optional)',
  `is_deleted` int(1) DEFAULT 0 COMMENT 'deletion flag'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='the model for the form object';

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

CREATE TABLE `records` (
  `id` int(10) NOT NULL,
  `record_number` varchar(100) DEFAULT NULL COMMENT 'uniue identifer for the records (optional) designed as a custom field.',
  `form_id` int(10) NOT NULL COMMENT 'form identifier',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(10) NOT NULL COMMENT 'staff that created the record',
  `is_deleted` int(1) DEFAULT 0 COMMENT 'deletion flag'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='These are the records created when ever a form is submitted. each submission must have one unique record t identify it';

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) NOT NULL,
  `role_name` varchar(60) DEFAULT NULL COMMENT 'name of the role',
  `permissions` varchar(255) DEFAULT NULL COMMENT 'permissions of a roll(Recomendation : should submit a json array)',
  `is_deleted` int(1) DEFAULT 0 COMMENT 'deletion flag',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This model defines the roles available in the system.  each role has a least of predefined or programmed permissions.';

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(10) NOT NULL,
  `staff_name` varchar(60) DEFAULT NULL COMMENT 'name of the staff member',
  `email` varchar(150) DEFAULT NULL COMMENT 'email for staff member. each email is unique',
  `password` varchar(255) DEFAULT NULL COMMENT 'passord used to secure the staff members account',
  `contact` varchar(20) DEFAULT NULL COMMENT 'the phone number of the staff member',
  `profile_picture` timestamp NULL DEFAULT NULL COMMENT 'the profile picture of the staff member',
  `is_deleted` int(1) DEFAULT 0 COMMENT 'deletion flag',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='this is a basic modal of all the  staff/users of the system';

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles`
--

CREATE TABLE `staff_roles` (
  `id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL COMMENT 'staff_identifier',
  `role_id` int(10) NOT NULL COMMENT 'role_identifier',
  `is_deleted` int(1) DEFAULT 0 COMMENT 'deletion flag',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This model represents the role assignment in the system';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staff_activity_logs` (`staff_id`);

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_can_have_multiple_entries` (`field_id`),
  ADD KEY `record_can_have_entries` (`record_id`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staff_can_create_a_form_field` (`created_by`),
  ADD KEY `form_can_have_as_many_fields` (`form_id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `form_name` (`form_name`),
  ADD KEY `staff_can_create_many_forms` (`created_by`);

--
-- Indexes for table `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `record_number` (`record_number`),
  ADD KEY `staff_can_create_many_records` (`created_by`),
  ADD KEY `form_can_have_many_records` (`form_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_name` (`role_name`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKstaff_role483650` (`staff_id`),
  ADD KEY `staff_can_be_assigned_particular_role` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `entries`
--
ALTER TABLE `entries`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_roles`
--
ALTER TABLE `staff_roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD CONSTRAINT `staff_activity_logs` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`);

--
-- Constraints for table `entries`
--
ALTER TABLE `entries`
  ADD CONSTRAINT `field_can_have_multiple_entries` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `record_can_have_entries` FOREIGN KEY (`record_id`) REFERENCES `records` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `form_can_have_as_many_fields` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `staff_can_create_a_form_field` FOREIGN KEY (`created_by`) REFERENCES `staff` (`id`);

--
-- Constraints for table `forms`
--
ALTER TABLE `forms`
  ADD CONSTRAINT `staff_can_create_many_forms` FOREIGN KEY (`created_by`) REFERENCES `staff` (`id`);

--
-- Constraints for table `records`
--
ALTER TABLE `records`
  ADD CONSTRAINT `form_can_have_many_records` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`),
  ADD CONSTRAINT `staff_can_create_many_records` FOREIGN KEY (`created_by`) REFERENCES `staff` (`id`);

--
-- Constraints for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD CONSTRAINT `FKstaff_role483650` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  ADD CONSTRAINT `staff_can_be_assigned_particular_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
