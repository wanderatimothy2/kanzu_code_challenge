<?php

function getUniqueContentsFromFile($file){
    $contents = file_get_contents($file);
    $contents = explode(' ',$contents);
    return array_unique($contents,SORT_STRING);
}


// test 
$file = './test-file.txt';

$output = getUniqueContentsFromFile($file);

print_r($output);


function getOnlyPunctuationMarks($file){
    $contents = file_get_contents($file);
    preg_match_all("/(?![.=$'€%-])\p{P}/u",$contents,$punctuationMarks);
    return $punctuationMarks;

}


// test 2

$output = getOnlyPunctuationMarks($file);

print_r($output);