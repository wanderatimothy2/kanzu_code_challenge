<?php

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

function getEmailRegex($sentence){
     preg_match('/([a-z0-9.]+)@([a-z.]+)/',$sentence,$matches);
    return count($matches)? $matches[0] : null;
}

echo "Email : ". getEmailRegex($paragraph);


function getPhoneNumberRegex($sentence){
    preg_match('/([0-9]+)/',$sentence,$matches);
   return count($matches)? $matches[0] : null;
}

echo  "<br> Phone Number :". getPhoneNumberRegex($paragraph);

function getUrlRegex($sentence){
    preg_match('/(:?https|http):\/\/([-a-zA-z0-9.&?\/#]+)/',$sentence,$matches);
   return count($matches)? $matches[0] : null;
}

echo  "<br> Url :". getUrlRegex($paragraph);




// Without regular expressions
echo "<br/> <br/> -- Without regex -- <br/>";

function getEmail($sentence){
    
    $chunks = explode(' ',$sentence);
    foreach($chunks as $chunk){
        $chunk = trim(str_replace([',:'],'',$chunk));
        if(filter_var($chunk,FILTER_VALIDATE_EMAIL) != false){
            return $chunk;
        }
    }
    return null;

}

echo "Email : ". getEmail($paragraph);

function getPhoneNumber($sentence){
    
    $chunks = explode(' ',$sentence);
    foreach($chunks as $chunk){
        $chunk = trim(str_replace([' ',',',':','*', '+','-','^'],'',$chunk));
        if(is_numeric($chunk) && strlen($chunk) == 12){
            return $chunk;
        }
    }
    return null;

}

echo  "<br> Phone Number :". getPhoneNumber($paragraph);


function getUrl($sentence){
    
    $chunks = explode(' ',$sentence);
    foreach($chunks as $chunk){
        $chunk = trim(str_replace([',','*','+','^'],'',$chunk));
        if(filter_var($chunk,FILTER_VALIDATE_URL) != false){
            return $chunk;
        }
    }
    return null;

}
echo  "<br> Url :". getUrl($paragraph);
