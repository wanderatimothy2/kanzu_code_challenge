## Task Management App API - documentation
## Usage

# Run the webserver on port 8000
php artisan serve
```

## Routes

```
# Public


POST   /api/v1/login
@body: email, password

POST   /api/v1/register
@body: name, email, password, password_confirmation


# Protected

POST   /api/v1/project/create
@body: name, description

PUT   /api/v1/project/:id
@body: ?name, ?description, 

POST   /api/v1/project/:project/milestone
@body: name, tasks

PUT   /api/v1/milestone/:id
@body: name, tasks

POST   /api/v1/changeStatus/:project_id
@body: status 
NB: allowed statuses (awaiting-start,on-hold,in-progress,completed)

POST   /api/v1/assign/:user_id/project/:project_id
@body: role 

GET   /api/v1/assigned/projects/:user_id
returns the projects assigned to a specific user (They must be assigned a role to the project )
GET   /api/v1/assigned/projects
NB: returned projects will be the projects assigned to the user currently authenticated

POST    /api/v1/logout