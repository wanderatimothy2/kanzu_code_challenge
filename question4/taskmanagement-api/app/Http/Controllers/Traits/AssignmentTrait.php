<?php
namespace App\Http\Controllers\Traits;

use App\Models\Project;
use App\Models\ProjectAssignment;

trait AssignmentTrait {


    public function checkIfUserIsAssignedToTheProject(Project $project  ){

        $assignments = ProjectAssignment::where(['user_id' => auth()->user()->id,'project_id' => $project->id])->get();

        return (count($assignments) > 0);
    }


    public function getAssignedProjects($filter){
        $assignments = ProjectAssignment::where($filter)->get(['project_id']);
        return $assignments;
    }


    public function getProjectManager(Project $project){
        $assignments = ProjectAssignment::where(['role' => 'project manager','project_id' => $project->id])->get();
        return (count($assignments) > 0)? $assignments[0] : null;
    }


    public function getUserRoleOnProject(Project $project , $user_id){
        $assignments = ProjectAssignment::where(['user_id' => $user_id,'project_id' => $project->id])->get();
        return (count($assignments) > 0)? $assignments[0] : null;
    }
}