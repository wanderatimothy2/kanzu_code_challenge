<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\AssignmentTrait;
use App\Http\Resources\ProjectCollection;
use App\Http\Resources\ProjectResource;
use App\Models\Milestone;
use App\Models\Project;
use App\Models\ProjectAssignment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class MainAppController extends Controller
{
    use AssignmentTrait;

    public function register(Request $request){
        $credentials = $request->only('password', 'email' ,'name');
        //valid credential
        $validator = Validator::make($credentials, [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'password' => 'required|min:8|confirmed'
        ]);

       //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['message' => 'failure', 'data' => $validator->errors()],  Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response()->json(['status' => 'success' , 'message' => 'account registered successfully'],201);

    }

    //authenticate the user to access the api
    public function login(Request $request){

        
        $credentials = $request->only('password', 'email');
        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required',
            'password' => 'required'
        ]);


       //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['message' => 'failure', 'data' => $validator->errors()],  Response::HTTP_BAD_REQUEST);
        }

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid credentials could not authenticate.',
            ], 401);
        }


            $user = User::where('email', $request->email)->first();

            return response()->json([
                'status' => true,
                'message' => 'success',
                'access_token' => $user->createToken("manager-token")->plainTextToken
            ], 200);
    }


    public function logout(){
        auth()->user()->tokens()->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'user session successfully terminated'
        ],200);
    }




    public function createProject(Request $request){

       

        $data = $request->only('name', 'description','milestones');
        //valid credential
        $validator = Validator::make($data, [
            'name' => 'required|unique:projects,name',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'failure', 'data' => $validator->errors()],  Response::HTTP_BAD_REQUEST);
        }

        $project = Project::create([
            'name' => $request->name,
            'description' =>    $request->description,
            'status' => 'awaiting-start',
            'user_id' => auth()->user()->id
        ]);
        // the project creator is by default a developer
        if($project){
            $assign = ProjectAssignment::create([
                'user_id' => auth()->user()->id,
                'project_id' => $project->id,
                'role' => 'developer',
                // 'permissions' => "[*]"
            ]);
        }

        return response()->json(['status' => 'success' , 'message' => 'project created  successfully'],201);


    }

    // update project
    public function updateProject(Request $request,Project $Project){

        if(!$this->checkIfUserIsAssignedToTheProject($Project)){
            return response()->json(['status' => 'denied' ,'message' => 'you don\'t have enough permissions'],403);
        }

        $data = $request->only('name', 'description','milestones');
        //valid credential
        $validator = Validator::make($data, [
            'name' => 'required|unique:projects,name',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'failure', 'data' => $validator->errors()],  Response::HTTP_BAD_REQUEST);
        }

        $Project->update([
            'name' => $request->name,
            'description' =>    $request->description,
        ]);


        return response()->json(['status' => 'success' , 'message' => 'project updated  successfully'],200);


    }

    // change the project status
    public function updateProjectStatus(Request $request , Project $Project){

        
        if(!$this->checkIfUserIsAssignedToTheProject($Project)){
            return response()->json(['status' => 'denied' ,'message' => 'you don\'t have enough permissions'],403);
        }

        $data = $request->only('status');
        //valid credential
        $validator = Validator::make($data, [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'failure', 'data' => $validator->errors()],  Response::HTTP_BAD_REQUEST);
        }

        $to_status = $request->status;
        
        // state machine

        switch($Project->status){
            case 'awaiting-start':
                if(!in_array($to_status,['in-progress', 'on-hold'])){
                    return response()->json(['message' => 'failure', 'data' => ['status' => 'can not move from '.$Project->status . ' to '. $to_status ]],  Response::HTTP_BAD_REQUEST);
                }
            break;
            case 'in-progress':
                if(!in_array($to_status,['on-hold' , 'completed'])){
                    return response()->json(['message' => 'failure', 'data' => ['status' => 'can not move from '.$Project->status . ' to '. $to_status ]],  Response::HTTP_BAD_REQUEST);
                }
            break;
            case 'on-hold':
                if(!in_array($to_status,['in-progress'])){
                    return response()->json(['message' => 'failure', 'data' => ['status' => 'can not move from '.$Project->status . ' to '. $to_status ]],  Response::HTTP_BAD_REQUEST);
                }
            break;
            case 'completed':
                    return response()->json(['message' => 'failure', 'data' => ['status' => 'project is already completed' ]],  Response::HTTP_BAD_REQUEST);
            break;
            default:
              return response()->json(['message' => 'failure', 'data' => ['status' => 'unknown status']],  Response::HTTP_BAD_REQUEST);
            break;
        }
        $project_manager = $this->getProjectManager($Project);
        if($to_status == 'completed' && (!is_null($project_manager)  && $project_manager->user_id != auth()->user()->id)){
            // only a project manager can complete
                return response()->json(['status' => 'denied' ,'message' => 'only a project manager can mark a project completed'],403);
            
        }

        $Project->update(['status' => $to_status]);

        return response()->json(['status' => 'success' , 'message' => 'project status updated  successfully'],200);


    }


    // creating a milestone
    public function createMilestone(Request $request, Project $Project){

        if(!$this->checkIfUserIsAssignedToTheProject($Project)){
            return response()->json(['status' => 'denied' ,'message' => 'you don\'t have enough permissions'],403);
        }

        $data = $request->only('name','tasks');
        
        $validator = Validator::make($data, [
            'name' => 'required',
            'tasks' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'failure', 'data' => $validator->errors()],  Response::HTTP_BAD_REQUEST);
        }

        $milestone = Milestone::create([
            'name' => $request->name,
            'tasks' => $request->tasks,
            'project_id' => $Project->id
        ]);

        
        return response()->json(['status' => 'success' , 'message' => 'milestone created  successfully'],201);

    }

    // update milestone

    public function updateMilestone(Request $request, Milestone $Milestone){

        $data = $request->only('name');
        
        $validator = Validator::make($data, [
            'name' => 'required',
            'tasks' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'failure', 'data' => $validator->errors()],  Response::HTTP_BAD_REQUEST);
        }

        $Milestone->update([
            'name' => $request->name,
            'tasks' => $request->tasks,
        ]);

        
        return response()->json(['status' => 'success' , 'message' => 'milestone updated  successfully'],200);

    }


    public function assignToProject(Request $request, User $User ,  Project $Project  ){

        //validating the possible roles
        if(!in_array($request->role,['engineer','developer','project manager'])){
            return response()->json(['message' => 'failure', 'data' => ['role' => 'role does not exist']],  Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $project_manager = $this->getProjectManager($Project);
        $user_role = $this->getUserRoleOnProject($Project,$User->id);
        // condition to check if the user is already a assigned a role on this project
        if(!is_null($user_role)){
            // in case true change the role the user has  to a new role
            if($request->role == 'engineer' && (!is_null($project_manager) &&  $project_manager->user_id != auth()->user()->id)){
                return response()->json(['status' => 'denied' ,'message' => 'only a project manager can assign an engineer to a project'],403);
            }
            $user_role->update(['role' => $request->role]);
            return response()->json(['status' => 'success' ],200);
        }else{
            // check current user role on project in the cse the role being assigned is 'engineer'
            if($request->role == 'engineer' && (!is_null($project_manager) &&  $project_manager->user_id != auth()->user()->id)){
                return response()->json(['status' => 'denied' ,'message' => 'only a project manager can assign an engineer to a project'],403);
            }
            // a sign use to the project
            $assignement = ProjectAssignment::create([
                'role' => $request->role,
                'project_id' => $Project->id,
                'user_id' => $User->id,
                // 'permissions' => "[]" // permission field to be eliminated 
            ]);

            return response()->json(['status' => 'success' ],200);

        }
       
      

    }

    public function getProjects(Request $request){

        $projects = $this->getAssignedProjects(['user_id' => auth()->user()->id]);
        $result = [];
        foreach($projects as $project){
            $result[] = new ProjectResource(Project::find($project->project_id));
        }
        return response()->json(['data' => $result ],200);

    }

    // projects assigned to a specific engineer

    public function getProjectsAssignedToUser(Request $request, User $User){
        $projects = $this->getAssignedProjects(['user_id' => $User->id]);
        $result = [];
        foreach($projects as $project){
            $result[] = new ProjectResource(Project::find($project->project_id));
        }
        return response()->json(['data' => $result ],200);
    }


    public function getRoles(){
        return response()->json(['data' => ['engineer','project manager', 'developer'] ],200);
    }

   



    

}
