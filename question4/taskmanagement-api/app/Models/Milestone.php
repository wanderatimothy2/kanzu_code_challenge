<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'tasks','project_id'];

    public function Project(){
        return $this->belongsTo(Project::class,"project_id","id");
    }
}
