<?php

use App\Http\Controllers\MainAppController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){
    Route::post('/login',[MainAppController::class,"login"]);
    Route::post('/register',[MainAppController::class,"register"]);
    Route::group(['middleware' => 'auth:sanctum'], function(){
        Route::post('/logout',[MainAppController::class,"logout"]);
        Route::post('/project/create',[MainAppController::class,"createProject"]);
        Route::put('/project/{Project}',[MainAppController::class,"updateProject"]);
        Route::post('/changeStatus/{Project}',[MainAppController::class,"updateProjectStatus"]);
        Route::post('/project/{Project}/milestone',[MainAppController::class,"createMilestone"]); 
        Route::put('/milestone/{Milestone}',[MainAppController::class,"updateMilestone"]);
        Route::post('/project/{Project}/changeStatus',[MainAppController::class,"createProject"]);
        Route::post('/assign/{User}/project/{Project}',[MainAppController::class,"assignToProject"]);
        Route::get('/assigned/projects',[MainAppController::class,"getProjects"]);
        Route::get('/assigned/projects/{User}',[MainAppController::class,"getProjectsAssignedToUser"]);
        Route::get('/roles',[MainAppController::class,"getRoles"]);
    });

});


