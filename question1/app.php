<?php

// generating an array of numbers from 0 to 100;

$numbers =  range(0,100);

// this function checks if a number is a perfect square
function isPerfectSquare($number){
     return (pow(((int)sqrt($number)),2) == $number);
}
// this function checks if a number falls with in the fibonacci serries
function isFibonacciTerm($number){
    return (isPerfectSquare(5  * pow($number,2) + 4) || isPerfectSquare(5  * pow($number,2) + 4));
}

// this  function returns the nth term oof the fibonacci serries

function getNthTermOfTheSeries($n){
    if($n == 0 || $n == 1){
        return $n;
    }else{
         $first = 0;
         $second = 1;
         $next = $first + $second; 
         for($i=3;$i<$n;$i++){
            $first = $second;
            $second = $next;
            $next =  $first + $second;
         }
         return $next; 
    }
}



// filtering the items in the array to return only greater or seventh term

$numbers = array_filter($numbers,function($number){
    return (isFibonacciTerm($number) && ($number >= getNthTermOfTheSeries(7)) );
});

// sorting the numbers in the array in descending order;
rsort($numbers);

// outputting the numbers;
echo implode(',',$numbers);